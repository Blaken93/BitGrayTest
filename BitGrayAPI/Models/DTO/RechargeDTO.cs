﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.Models.DTO
{
    public class RechargeDTO
    {
        public double Amount { get; set; }
        public int Phonenumber { get; set; }
        public int idUser { get; set; }
        public double balance { get; set; }
        public double bonus { get; set; }
        public string bonusType { get; set; }
    }
}