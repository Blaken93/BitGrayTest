﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.Models.DTO
{
    public class SpendMinutesDTO
    {
        public double minutes { get; set; }
        public int Phonenumber { get; set; }
        public int idUser { get; set; }
    }
}