﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.Models.DTO
{
    public class PriceDTO
    {
        public double pricePerMinute { get; set; }
        public double discount { get; set; }
    }
}