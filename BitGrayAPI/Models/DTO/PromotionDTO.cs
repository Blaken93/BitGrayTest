﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.Models.DTO
{
    public class PromotionDTO
    {
        public double BonusAmount { get; set; }
        public String Type { get; set; }

    }
}