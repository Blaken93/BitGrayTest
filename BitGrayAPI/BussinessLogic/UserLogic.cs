﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BitGrayAPI.Models;

namespace BitGrayAPI.BussinessLogic
{
    public class UserLogic
    {
        bitgrayDBEntities db = new bitgrayDBEntities();
        public void CreateUser(users user)
        {
            db.users.Add(user);
            db.data.Add(new data()
            {
                users = user
            });
            db.SaveChanges();
        }

        public void DeleteUserById(int UserId)
        {
            var userToRemove = db.users.First(i => i.idUser == UserId);
            db.users.Remove(userToRemove);
            db.SaveChanges();
        }

        public users UpdateUser(users newUser, int idUser)
        {
            var entity = db.users.Find(idUser);
            newUser.idUser = idUser;
            db.Entry(entity).CurrentValues.SetValues(newUser);
            db.SaveChanges();
            return entity;
        }

        public users GetUserById(int UserId)
        {
            return db.users.First(i => i.idUser == UserId);
        }
        public users GetUserByPhonenumber(int Phonenumber)
        {
            return db.users.First(i => i.PhoneNumber == Phonenumber);
        }

        public List<users> GetUsers()
        {
            return db.users.ToList();
        }
    }
}