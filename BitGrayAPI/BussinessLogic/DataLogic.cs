﻿using BitGrayAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.BussinessLogic
{
    public class DataLogic
    {
        bitgrayDBEntities db = new bitgrayDBEntities();
        public void CreateData(data user)
        {
            db.data.Add(user);
            db.SaveChanges();
        }

        public void DeleteDataByUserId(int UserId)
        {
            var userToRemove = db.data.First(i => i.idUser== UserId);
            db.data.Remove(userToRemove);
            db.SaveChanges();
        }

        public data UpdateData(data newData)
        {
            var entity = db.data.First(i => i.idUser == newData.idUser && i.PhoneNumber == newData.PhoneNumber);
            db.Entry(entity).CurrentValues.SetValues(newData);
            db.SaveChanges();
            return entity;
        }

        public data GetDataByUserId(int UserId, int PhoneNumber)
        {
            return db.data.First(i => i.idUser == UserId && i.PhoneNumber==PhoneNumber);
        }
        public List<data> GetDataByUserId(int UserId)
        {
            return db.data.Where(i => i.idUser == UserId).ToList();
        }

        public List<data> GetData()
        {
            return db.data.ToList();
        }
    }
}
