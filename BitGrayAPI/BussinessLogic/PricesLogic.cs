﻿using BitGrayAPI.Models;
using BitGrayAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.BussinessLogic
{
    
    public class PricesLogic
    {
        bitgrayDBEntities db = new bitgrayDBEntities();
        public PriceDTO GetPrice()
        {
            var price= db.prices.Find(1);
            return new PriceDTO() { pricePerMinute= price.PricePerMinute,discount=price.Discount };
        }

        public PriceDTO ChangePrice(PriceDTO data)
        {
            var entity = db.prices.Find(1);
            db.Entry(entity).CurrentValues.SetValues(new prices() { IdPrice=1,Discount=data.discount,PricePerMinute=data.pricePerMinute});
            db.SaveChanges();
            entity = db.prices.Find(1);
            return new PriceDTO() { pricePerMinute = entity.PricePerMinute, discount = entity.Discount };
        }
    }
}