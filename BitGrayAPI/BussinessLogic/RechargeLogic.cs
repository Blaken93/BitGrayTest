﻿using BitGrayAPI.Models;
using BitGrayAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.BussinessLogic
{
    public class RechargeLogic
    {
        bitgrayDBEntities db = new bitgrayDBEntities();
        public int CreateRecharge(recharges recharge)
        {
            db.recharges.Add(recharge);
            return db.SaveChanges();
        }

        public void DeleteRechargeById(int RechargeId)
        {
            var rechargeToRemove = db.recharges.First(i => i.idRecharge == RechargeId);
            db.recharges.Remove(rechargeToRemove);
            db.SaveChanges();
        }

        public recharges UpdateRecharge(recharges newRecharge, int idRecharge)
        {
            var entity = db.recharges.Find(idRecharge);
            newRecharge.idRecharge = idRecharge;
            db.Entry(entity).CurrentValues.SetValues(newRecharge);
            db.SaveChanges();
            return entity;
        }

        public List<recharges> GetRechargeByUserId(int UserId)
        {
            return db.recharges.Where(i => i.idUser == UserId).ToList();
        }
       
        public List<recharges> GetRecharges()
        {
            return db.recharges.ToList();
        }
        public double AverageAmountRecharges(int UserId,int Phonenumber)
        {
            var amount = db.recharges.Where(i => i.idUser == UserId && i.PhoneNumber==Phonenumber);
            return amount.Count()==0 ? 0 : amount.Average(i => i.Amount);
        }
        public double TodayMinimumRecharge(int UserId, int Phonenumber)
        {
            var amount= db.recharges.Where(i => i.idUser == UserId && i.PhoneNumber == Phonenumber && i.OperationDate >= DateTime.Today);
            return amount.Count() == 0 ? 0 : amount.Min(i => i.Amount);
        }
        public double LastWeekMinAverageAmountRecharges(int UserId, int Phonenumber)
        {
            var lastWeekDate = DateTime.Today.AddDays(-7);
            var lastWeekMinRecharges= db.recharges.Where(i => i.idUser == UserId && i.PhoneNumber == Phonenumber && i.OperationDate >= lastWeekDate)
                .GroupBy(g => g.OperationDate, i=> i.Amount ).Select(g => new
                {
                    UserId= g.Key,
                    Amount = g.Min()
                });
            return lastWeekMinRecharges.Count() == 0 ? 0 : lastWeekMinRecharges.Average(i=> i.Amount);
        }

        public bool todayBonusApplied(int UserId, int Phonenumber)
        {
            var bonusRecharge=db.recharges.FirstOrDefault(i => i.idUser == UserId && i.PhoneNumber == Phonenumber && i.OperationDate == DateTime.Today && i.BonusAmount != 0.0);
            return bonusRecharge == null ? true : false;
        }
    }
}
