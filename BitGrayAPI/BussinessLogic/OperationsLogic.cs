﻿using BitGrayAPI.Models;
using BitGrayAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitGrayAPI.BussinessLogic
{
    public class OperationsLogic
    {
        UserLogic _userLogic = new UserLogic();
        RechargeLogic _rechargeLogic = new RechargeLogic();
        DataLogic _dataLogic = new DataLogic();
        PricesLogic _priceLogic = new PricesLogic();
        public RechargeDTO Recharge(RechargeDTO data)
        {
            var bonus = PromotionAmount(data);
            double bonusAmount = bonus.BonusAmount;
            recharges recharge = new recharges() {
                Amount = data.Amount,
                idUser = data.idUser,
                OperationDate = DateTime.Today,
                BonusAmount = bonusAmount,
                PhoneNumber=data.Phonenumber};
            data userData= _dataLogic.GetDataByUserId(data.idUser,data.Phonenumber);
            userData.MoneyRecharge += data.Amount;
            userData.Balance += data.Amount + bonusAmount;
            _rechargeLogic.CreateRecharge(recharge);
            data.balance = userData.Balance;
            data.bonus = bonusAmount;
            data.bonusType = bonus.Type;
            return data;
         }

        public String spendMinutes( SpendMinutesDTO minutesSpendByUser)
        {
            PriceDTO price = _priceLogic.GetPrice();
            data userData = _dataLogic.GetDataByUserId(minutesSpendByUser.idUser, minutesSpendByUser.Phonenumber);
            double wastedMoney = WastedMoney(minutesSpendByUser, price,userData.Balance);
            String resul = "";
            if (userData.Balance <= wastedMoney)
            {
                userData.Balance = 0;
                userData.Spend += wastedMoney;
                userData.Minutes += minutesSpendByUser.minutes;
                resul = "Your Balance is O. Please recharge";
            }
            else
            {
                userData.Balance -= wastedMoney;
                userData.Spend += wastedMoney;
                userData.Minutes += minutesSpendByUser.minutes;
                resul = "Your Balance is " + userData.Balance;
            }
            _dataLogic.UpdateData(userData);
            return resul;
        }

        private double WastedMoney(SpendMinutesDTO minutesSpendByUser, PriceDTO price,double balance)
        {
            double discount = price.pricePerMinute - (minutesSpendByUser.minutes * price.discount) / 60;
            double wastedMoney = minutesSpendByUser.minutes * price.pricePerMinute;
            return balance==0? wastedMoney : wastedMoney-discount;
        }

        public PromotionDTO PromotionAmount(RechargeDTO data)
        {
            
            double avgRecharge = _rechargeLogic.AverageAmountRecharges(data.idUser, data.Phonenumber);
            double minRechargeDay = _rechargeLogic.TodayMinimumRecharge(data.idUser, data.Phonenumber);
            return VerifyPromotions(data, avgRecharge, minRechargeDay);
        }

        private PromotionDTO VerifyPromotions(RechargeDTO data,  double avgRecharge, double minRechargeDay)
        {
            PromotionDTO promotion = new PromotionDTO();
            promotion.BonusAmount = 0;
            promotion.Type = "N/A";
            if (!applyBonus(data))
            {
                promotion.BonusAmount = 0;
                promotion.Type = "Bonus Used Today";
            }
            else if (data.Amount > avgRecharge)
            {
                promotion.BonusAmount = data.Amount * 0.10;
                promotion.Type = "10% of recharge Amount";
            }
            else if (data.Amount < minRechargeDay)
            {
                promotion.BonusAmount = _rechargeLogic.LastWeekMinAverageAmountRecharges(data.idUser, data.Phonenumber) * 0.05;
                promotion.Type = "5% of Last week minimun average";
            }
            return promotion;
        }

        public bool applyBonus(RechargeDTO recharge)
        {
            return _rechargeLogic.todayBonusApplied(recharge.idUser,recharge.Phonenumber);
        }
    }
}