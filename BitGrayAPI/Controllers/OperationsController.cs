﻿using BitGrayAPI.BussinessLogic;
using BitGrayAPI.Models;
using BitGrayAPI.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BitGrayAPI.Controllers
{
    public class OperationsController : ApiController
    {
        OperationsLogic _operationsLogic = new OperationsLogic();
        PricesLogic _pricesLogic = new PricesLogic();   
        public RechargeDTO Recharge([FromBody]RechargeDTO data)
        {
            return _operationsLogic.Recharge(data);
        }

        public PromotionDTO PromotionAmount([FromBody]RechargeDTO data)
        {
            return _operationsLogic.PromotionAmount(data);
        }

        public PriceDTO GetPrices()
        {
            return _pricesLogic.GetPrice();
        }

        public PriceDTO ChangePrice([FromBody]PriceDTO data)
        {
            return _pricesLogic.ChangePrice(data);
        }

        public String spendMinutes([FromBody]SpendMinutesDTO data)
        {
           return _operationsLogic.spendMinutes(data);
        }
    }
}
