﻿using BitGrayAPI.BussinessLogic;
using BitGrayAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BitGrayAPI.Controllers
{
    public class RechargeController : ApiController
    {
        RechargeLogic logic = new RechargeLogic();
        // GET: api/Recharge
        public IEnumerable<recharges> Get()
        {
            return logic.GetRecharges();
        }
        public int GetRecharge(int id)
        {
            return 0;
        }
    

    // GET: api/Recharge/5
        public List<recharges> Get(int id)
        {
            return logic.GetRechargeByUserId(id);
        }

        // POST: api/Recharge
        public void Post([FromBody]recharges value)
        {
            logic.CreateRecharge(value);
        }

        // PUT: api/Recharge/5
        public void Put(int id, [FromBody]recharges value)
        {
            logic.UpdateRecharge(value,id);
        }

        // DELETE: api/Recharge/5
        public void Delete(int id)
        {
            logic.DeleteRechargeById(id);
        }
    }
}
