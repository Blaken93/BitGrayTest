﻿using BitGrayAPI.BussinessLogic;
using BitGrayAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BitGrayAPI.Controllers
{
    public class DataController : ApiController
    {
        DataLogic logic = new DataLogic();
        // GET: api/Data
        public IEnumerable<data> Get()
        {
            return logic.GetData() ;
        }

        // GET: api/Data/5
        public List<data> Get(int id)
        {
            return logic.GetDataByUserId(id);
        }

        // POST: api/Data
        public void Post([FromBody]data value)
        {
            logic.CreateData(value);
        }

        // PUT: api/Data/5
        public data Put([FromBody]data value)
        {
            return logic.UpdateData(value);
        }

        // DELETE: api/Data/5
        public void Delete(int id)
        {
            logic.DeleteDataByUserId(id);
        }
    }
}
