﻿using BitGrayAPI.BussinessLogic;
using BitGrayAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BitGrayAPI.Controllers
{
    public class UserController : ApiController
    {
        UserLogic logic = new UserLogic();
        // GET: api/User
        public IEnumerable<users> Get()
        {
            return logic.GetUsers();
        }

        // GET: api/User/5
        public users Get(int id)
        {
            return logic.GetUserById(id);
        }

        // POST: api/User
        public void Post([FromBody]users value)
        {
            logic.CreateUser(value);
        }

        // PUT: api/User/5
        public users Put(int id, [FromBody]users value)
        {
            return logic.UpdateUser(value,id);
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
            logic.DeleteUserById(id);
        }
    }
}
