-- MySQL Script generated by MySQL Workbench
-- Sat Aug 19 00:47:54 2017
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bitgrayinterview
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bitgrayinterview
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bitgrayinterview` DEFAULT CHARACTER SET utf8 ;
USE `bitgrayinterview` ;

-- -----------------------------------------------------
-- Table `bitgrayinterview`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitgrayinterview`.`users` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT,
  `PhoneNumber` INT NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NULL DEFAULT NULL,
  `Password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idUser`, `PhoneNumber`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bitgrayinterview`.`data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitgrayinterview`.`data` (
  `idUser` INT(11) NOT NULL,
  `Spend` DOUBLE NOT NULL,
  `Balance` DOUBLE NOT NULL,
  `Minutes` DOUBLE NOT NULL,
  `MoneyRecharge` DOUBLE NOT NULL,
  `PhoneNumber` INT NOT NULL,
  PRIMARY KEY (`idUser`, `PhoneNumber`),
  CONSTRAINT `UserDataRelationship`
    FOREIGN KEY (`idUser` , `PhoneNumber`)
    REFERENCES `bitgrayinterview`.`users` (`idUser` , `PhoneNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bitgrayinterview`.`recharges`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitgrayinterview`.`recharges` (
  `idRecharge` INT(11) NOT NULL AUTO_INCREMENT,
  `idUser` INT(11) NOT NULL,
  `Amount` DOUBLE NOT NULL,
  `OperationDate` DATE NOT NULL,
  `PhoneNumber` INT NOT NULL,
  `BonusAmount` DOUBLE NULL DEFAULT 0,
  PRIMARY KEY (`idRecharge`),
  INDEX `UserRechargeRelationship_idx` (`idUser` ASC, `PhoneNumber` ASC),
  CONSTRAINT `UserRechargeRelationship`
    FOREIGN KEY (`idUser` , `PhoneNumber`)
    REFERENCES `bitgrayinterview`.`users` (`idUser` , `PhoneNumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `bitgrayinterview`.`Prices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitgrayinterview`.`Prices` (
  `IdPrice` INT NOT NULL,
  `PricePerMinute` DOUBLE NOT NULL,
  `Discount` DOUBLE NOT NULL,
  PRIMARY KEY (`IdPrice`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
